// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {ACA} from '../app/models/aca';

export const environment = {
    production: false,
     OTM_INSTANCE: 'https://dev.co3.ontomap.eu',
    // OTM_INSTANCE: 'http://localhost:9000',
    FL_INSTANCE: 'https://api.co3-dev.firstlife.org',
    LF_INSTANCE: 'https://co3.liquidfeedback.com',
    USER_AUTH: {
        access_token: '9Ut3nllplbvGVSLK',
        auth_server: 'CO3UUM_DEV',
        member_id: '92@CO3UUM_DEV'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
