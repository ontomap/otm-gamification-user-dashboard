export class PaginatedQueryResult<T> {
    results: T[];
    totalCount: number;
    page: number;
    pageSize: number;
}
