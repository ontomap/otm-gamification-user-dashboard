export class AcaProfile {
    url: string;
    badgeList: BadgeList = new BadgeList();
    temporalVariableContainers: TemporalVariableContainer[] = [];

    static fromJson(json: any): AcaProfile {
        const acaProfile = new AcaProfile();
        acaProfile.url = json.url;
        if (json.badges) {
            acaProfile.badgeList = new BadgeList();
            if (json.badges.currentBadges) {
                json.badges.currentBadges.forEach(badge => {
                    const current = new AssignedBadge();
                    acaProfile.badgeList.currentBadges.push(current);
                    current.idBadge = badge.idBadge;
                    current.assignedDate = new Date(badge.assignedDate);
                    current.expiredDate = new Date(badge.expiredDate);
                });
            }
            if (json.badges.removedBadges) {
                json.badges.removedBadges.forEach(badge => {
                    const current = new AssignedBadge();
                    acaProfile.badgeList.removedBadges.push(current);
                    current.idBadge = badge.idBadge;
                    current.assignedDate = new Date(badge.assignedDate);
                    current.expiredDate = new Date(badge.expiredDate);
                });
            }
        }
        if (json.temporalVariableContainers) {
            json.temporalVariableContainers.forEach(tvc => {
                const container = new TemporalVariableContainer();
                acaProfile.temporalVariableContainers.push(container);
                container.timeframe = tvc.timeframe;
                container.start = new Date(tvc.start);
                container.end = new Date(tvc.end);
                container.variables = tvc.variables;
            });
        }
        return acaProfile;
    }

    hasBadge(idBadge: string): boolean {
        return this.badgeList.currentBadges.find(cb => cb.idBadge === idBadge) != null;
    }

    getContainer(timeframe: string): TemporalVariableContainer {
        return this.temporalVariableContainers.find(container => container.timeframe === timeframe);

    }
}


export class TemporalVariableContainer {
    timeframe: string;
    start: Date;
    end: Date;
    variables: { [key: string]: number };
}

export class BadgeList {
    currentBadges: AssignedBadge[] = [];
    removedBadges: AssignedBadge[] = [];
}

export class AssignedBadge {
    idBadge: string;
    assignedDate: Date;
    expiredDate: Date;
}
