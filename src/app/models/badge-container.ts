import {Badge, BadgePosition, BadgeStep} from './badge';

export abstract class BadgeContainer<T extends Badge> {
    private badgeList: T[];
    id: string;
    name: string;
    aca: string;
    color: string;

    protected constructor() {
        this.badgeList = [];
    }

    static fromJson(json: any): BadgeContainer<Badge> {
        let bc: BadgeContainer<Badge>;
        switch (json.type) {
            case 'ranking':
                bc = new RankingBadgeContainer();
                break;
            case 'aca':
                bc = new AcaBadgeContainer();
                break;
            default:
                bc = new IndividualBadgeContainer();
                break;
        }
        bc.name = json.name;
        bc.aca = json.aca;
        bc.id = json.id;
        json.badgeList.forEach(b => {
            let badge;
            if (bc instanceof RankingBadgeContainer) {
                badge = BadgePosition.fromJson(b);
            } else {
                badge = BadgeStep.fromJson(b);
            }
            badge.badgeContainer = bc;
            bc.badgeList.push(badge);
        });

        if (bc instanceof TemporalBadgeContainer) {
            bc.timeframe = json.timeframe;
            if (bc instanceof RankingBadgeContainer) {
                bc.variable = json.variable;
            }
        }
        return bc;
    }

    abstract getType(): string;

    getBadgeList(): T[] {
        return this.badgeList;
    }

    abstract getVariables(): string[];

    setBadgeList(badgeList: T[]): void {
        this.badgeList = badgeList;
    }
}

export abstract class TemporalBadgeContainer<B extends Badge> extends BadgeContainer<B> {
    timeframe: string;

    public getTimeframe(): string {
        return this.timeframe;
    }
}

export class IndividualBadgeContainer extends BadgeContainer<BadgeStep> {
    private type = 'individual';

    constructor() {
        super();
    }

    public getType(): string {
        return this.type;
    }

    public getBadgeList(): BadgeStep[] {
        return super.getBadgeList();
    }

    public getVariables(): string[] {
        const ret = {};
        this.getBadgeList().forEach(badge => {
            if (badge.preconditions.variables){
                badge.preconditions.variables.forEach(v => {
                    ret[v.variable] = true;
                });
            }
        });
        return Object.keys(ret);

    }
}

export class AcaBadgeContainer extends TemporalBadgeContainer<BadgeStep> {
    private type = 'aca';

    constructor() {
        super();
    }

    public getType(): string {
        return this.type;
    }

    public getBadgeList(): BadgeStep[] {
        return super.getBadgeList();
    }

    public getVariables(): string[] {
        const ret = {};
        this.getBadgeList().forEach(badge => {
            if (badge.preconditions.variables){
                badge.preconditions.variables.forEach(v => {
                    ret[v.variable] = true;
                });
            }
        });
        return Object.keys(ret);

    }
}

export class RankingBadgeContainer extends TemporalBadgeContainer<BadgePosition> {
    private type = 'ranking';
    variable: string;

    constructor() {
        super();
    }

    public getType(): string {
        return this.type;
    }

    public getVariable(): string {
        return this.variable;
    }

    public getBadgeList(): BadgePosition[] {
        return super.getBadgeList();
    }

    public getVariables(): string[] {
        return [this.variable];
    }
}
