export class Ranking {
  variable: string;
  aca: string;
  timeframe: string;
  rankList: Rank[];
  totalCount: number;
  endingDate: Date;
}

export class Rank{
  actorName: string;
  actor: number;
  value: number;
  position: number;
}
