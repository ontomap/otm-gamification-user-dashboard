
export class Rule {
  id: string;
  // tslint:disable-next-line:variable-name
  activity_type: string;
  rewards: { [role: string]: Variable[]; };
  activityObjectProperties: ObjectProperty[];
  referenceObjectProperties: ObjectProperty[];
  active: boolean;
  acas: string[];
  validFrom: number;
  validTo: number;
  maxEvDay: number;
  maxEvEver: number;

  constructor() {
  }

  static fromJson(json: any): Rule {
    const rule = new Rule();
    rule.id = json.id;
    rule.activity_type = json.activity_type;
    rule.active = json.active;
    if (json.rewards) {
      rule.rewards = {};
      Object.keys(json.rewards).forEach(role => {
        const vars = json.rewards[role];
        rule.rewards[role] = [];
        vars.forEach(v => {
          rule.rewards[role].push(new Variable(v.name, v.assignment));
        });
      });
    }
    if (json.activityObjectProperties && json.activityObjectProperties.length > 0) {
      rule.activityObjectProperties = [];
      json.activityObjectProperties.forEach(aop => {
        const newAOP = new ObjectProperty(aop.propertyName, aop.propertyValue);
        rule.activityObjectProperties.push(newAOP);
      });
    }
    if (json.referenceObjectProperties && json.referenceObjectProperties.length > 0) {
      rule.referenceObjectProperties = [];
      json.referenceObjectProperties.forEach(rop => {
        const newAOP = new ObjectProperty(rop.propertyName, rop.propertyValue);
        rule.referenceObjectProperties.push(newAOP);
      });
    }
    if (json.validFrom) {
      rule.validFrom = json.validFrom;
    }
    if (json.validTo) {
      rule.validTo = json.validTo;
    }
    if (json.maxEvDay) {
      rule.maxEvDay = json.maxEvDay;
    }
    if (json.maxEvEver) {
      rule.maxEvEver = json.maxEvEver;
    }
    return rule;
  }

  getVariableNames(): string[] {
    const names = [];
    Object.values(this.rewards).forEach(r => r.forEach(value => names.push(value.name)));
    return names;
  }
}

export enum Role {
  ACTOR = 'ACTOR', REFERENCE_OWNER = 'REFERENCE_OWNER'
}


export function getRoleToString(role: string): string {
  switch (role) {
    case Role.REFERENCE_OWNER:
      return 'Reference Owner';
      break;
    default:
      return 'Actor';
      break;
  }
}

export class Variable {
  name: string;
  assignment: number;


  constructor(name: string, assignment: number) {
    this.name = name;
    this.assignment = assignment;
  }
}

export class ObjectProperty {
  propertyName: string;
  propertyValue: string;


  constructor(propertyName: string, propertyValue: string) {
    this.propertyName = propertyName;
    this.propertyValue = propertyValue;
  }
}
