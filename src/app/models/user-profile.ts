export abstract class UserProfile {
    badges: Badges;
    actor: number;
    variables: { [varName: string]: number };
    maxVariablesValues: { [varName: string]: number };

    constructor() {
        this.badges = new Badges();
    }

    abstract getFullVariable(variable: string): number;

    abstract getPersona(aca?: string, group?: string): UserProfile;

    abstract hasBadge(idBadge: string): boolean;

    abstract getFullBadges(): AssignedBadge[];

}

export class MainPersona extends UserProfile {
    actor: number;
    parent: null;
    acaPersonas: AcaPersona[];

    static fromJson(json: any): MainPersona {
        const userProfile = new MainPersona();
        userProfile.actor = json.actor;
        userProfile.variables = json.variables;
        userProfile.maxVariablesValues = json.maxVariablesValues;
        if (json.badges) {
            userProfile.badges = Badges.fromJson(json.badges);
        }
        if (json.acaPersonas) {
            userProfile.acaPersonas = [];
            json.acaPersonas.forEach(acaPersona => {
                userProfile.acaPersonas.push(AcaPersona.fromJson(acaPersona, userProfile));
            });
        }
        return userProfile;
    }

    getPersona(aca: string, group: string): UserProfile {
        if (aca == null && group != null) {
            return null;
        }
        if (aca == null && group == null) {
            return this;
        }
        const persona = this.acaPersonas.find(ap => ap.aca === aca);
        return persona ? persona.getPersona(aca, group) : null;
    }

    hasBadge(idBadge: string): boolean {
        const badges = this.getFullBadges().map(b => b.idBadge);
        return badges.indexOf(idBadge) > -1;
    }

    getFullBadges(): AssignedBadge[] {
        let assignedBadges = this.badges.getAssignedBadges();
        this.acaPersonas.forEach(ap => assignedBadges = assignedBadges.concat(ap.getFullBadges()));
        return assignedBadges;
    }

    getFullVariable(variable: string): number {
        // tslint:disable-next-line:no-bitwise
        let count = this.variables[variable] | 0;
        this.acaPersonas.forEach(acaPersona => {
                count += acaPersona.getFullVariable(variable);
            }
        );
        return count;
    }
}

export class AcaPersona extends UserProfile {
    aca: string;
    parent: MainPersona;
    groupPersonas: GroupPersona[];

    static fromJson(json: any, mainPersona?: MainPersona): AcaPersona {
        const userProfile = new AcaPersona();
        userProfile.parent = mainPersona;
        userProfile.actor = json.actor;
        userProfile.aca = json.aca;
        userProfile.variables = json.variables;
        userProfile.maxVariablesValues = json.maxVariablesValues;
        if (json.badges) {
            userProfile.badges = Badges.fromJson(json.badges);
        }
        if (json.groupPersonas) {
            userProfile.groupPersonas = [];
            json.groupPersonas.forEach(gp => {
                userProfile.groupPersonas.push(GroupPersona.fromJson(gp, userProfile));
            });
        }
        return userProfile;
    }

    getPersona(aca: string, group: string): UserProfile {
        if (aca == null && group != null) {
            return null;
        }
        if (aca === this.aca && group == null) {
            return this;
        }
        const persona = this.groupPersonas.find(gp => gp.group === group);
        return persona ? persona.getPersona(aca, group) : null;
    }

    hasBadge(idBadge: string): boolean {
        const badges = this.getFullBadges().map(b => b.idBadge);
        return badges.indexOf(idBadge) > -1;
    }

    getFullBadges(): AssignedBadge[] {
        let assignedBadges = this.badges.getAssignedBadges();
        this.groupPersonas.forEach(ap => assignedBadges = assignedBadges.concat(ap.getFullBadges()));
        return assignedBadges;
    }

    getFullVariable(variable: string): number {
        // tslint:disable-next-line:no-bitwise
        let count = this.variables[variable] | 0;
        this.groupPersonas.forEach(groupPersona => {
                count += groupPersona.getFullVariable(variable);
            }
        );
        return count;
    }
}

export class GroupPersona extends UserProfile {
    group: string;
    parent: AcaPersona;

    static fromJson(json: any, parent: AcaPersona): GroupPersona {
        const userProfile = new GroupPersona();
        userProfile.parent = parent;
        userProfile.actor = json.actor;
        userProfile.variables = json.variables;
        userProfile.maxVariablesValues = json.maxVariablesValues;
        if (json.badges) {
            userProfile.badges = Badges.fromJson(json.badges);
        }
        return userProfile;
    }

    getPersona(aca: string, group: string): UserProfile {
        if (aca == null && group != null) {
            return null;
        }
        if (this.parent.aca === aca && group === this.group) {
            return this;
        }
    }

    hasBadge(idBadge: string): boolean {
        const badges = this.getFullBadges().map(b => b.idBadge);
        return badges.indexOf(idBadge) > -1;
    }

    getFullBadges(): AssignedBadge[] {
        return this.badges.getAssignedBadges();
    }


    getFullVariable(variable: string): number {
        // tslint:disable-next-line:no-bitwise
        return this.variables[variable] | 0;
    }
}


export class Badges {
    currentBadges: AssignedBadge[];
    removedBadges: AssignedBadge[];


    constructor() {
        this.currentBadges = [];
        this.removedBadges = [];
    }

    static fromJson(json: any): Badges {
        const badges = new Badges();
        if (json.currentBadges) {
            json.currentBadges.forEach(cb => {
                badges.currentBadges.push(AssignedBadge.fromJson(cb));
            });
        }
        if (json.removedBadges) {
            json.removedBadges.forEach(cb => {
                badges.removedBadges.push(AssignedBadge.fromJson(cb));
            });
        }
        return badges;
    }

    hasBadge(idBadge: string): boolean {
        // tslint:disable-next-line:max-line-length
        // return this.currentBadges.map(b => b.idBadge).indexOf(idBadge) > -1 || this.removedBadges.map(b => b.idBadge).indexOf(idBadge) > -1;
        return this.currentBadges.map(b => b.idBadge).indexOf(idBadge) > -1;
    }

    getAssignedBadges(): AssignedBadge[] {
        return this.currentBadges.concat(this.removedBadges);
    }

}


class AssignedBadge {
    idBadge: string;
    assignedDate: Date;
    expiredDate: Date;

    static fromJson(json: any): AssignedBadge {
        const ab = new AssignedBadge();
        ab.idBadge = json.idBadge;
        ab.assignedDate = new Date(json.assignedDate);
        ab.expiredDate = new Date(json.expiredDate);
        return ab;
    }
}
