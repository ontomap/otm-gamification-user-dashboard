import {Badge} from './badge';
import {Role, Rule} from './rule';

export class GamificationLog {
  id: string;
  actor: number;
  aca: string;
  group: string;
  date: Date;

  static fromJson(json: any): GamificationLog {
    let log: GamificationLog;
    if (json.rule) {
      log = new RuleLog();
    } else {
      log = new BadgeLog();
    }
    log.id = json.id;
    log.actor = json.actor;
    log.aca = json.aca;
    log.group = json.group;
    log.date = new Date(json.timestamp);

    if (log instanceof RuleLog) {
      log.event = json.event;
      log.rule = json.rule;
      log.role = json.role;
    } else if (log instanceof BadgeLog) {
      log.badge = json.badge;
    }
    return log;
  }

}

export class RuleLog extends GamificationLog {
  event: any;
  rule: Rule;
  role: Role;
}

export class BadgeLog extends GamificationLog {
  badge: Badge;
}


