import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AcaPersona, MainPersona, UserProfile} from '../models/user-profile';
import {map} from 'rxjs/operators';
import {BadgeContainer} from '../models/badge-container';
import {UUM} from '../models/uum';

@Injectable({
    providedIn: 'root'
})
export class UserProfileService {
    OTM_INSTANCE = environment.OTM_INSTANCE;
    LF_INSTANCE = environment.LF_INSTANCE;

    constructor(private http: HttpClient) {
    }

    getProfile(viewMode: string, aca?: string): Observable<UserProfile> {
        return this.http
            .get<MainPersona>(this.OTM_INSTANCE + '/api/v1/gamification/profile/user',
                {
                    headers: {
                        Authorization: 'Bearer ' + environment.USER_AUTH.access_token
                    },
                    params: {
                        viewmode: viewMode
                    }
                }).pipe(
                map((response: any) => {
                    if (!aca) {
                        return MainPersona.fromJson(response);
                    } else {
                        return AcaPersona.fromJson(response);
                    }

                    return null;
                }));
    }

    getUserName(actors: string[]): Observable<UUM[]> {
        return this.http
            .get<string>(this.LF_INSTANCE + '/api/1/member',
                {
                    headers: {
                        Authorization: 'Bearer ' + environment.USER_AUTH.access_token
                    },
                    params: {
                        id: actors.join(',')
                    }
                }).pipe(
                map((response: any) => {
                    if (response.result.length !== 0) {
                        return response.result;
                    }
                    return [];
                }));
    }


}
