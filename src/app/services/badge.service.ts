import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BadgeContainer} from '../models/badge-container';
import {Badge} from '../models/badge';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BadgeService {
  OTM_INSTANCE = environment.OTM_INSTANCE;

  constructor(private http: HttpClient) {
  }

  getBadgesAll(): Observable<BadgeContainer<Badge>[]> {
    return this.http
      .get<BadgeContainer<Badge>[]>(this.OTM_INSTANCE + '/api/v1/gamification/badgeContainers', {
      })
      .pipe(
        map((response: any) => {
          const bcList = [];
          response.forEach(bc => {
            bcList.push(BadgeContainer.fromJson(bc));
          });
          return bcList;
        })
      );
  }

  getBadgesByType(type: string): Observable<BadgeContainer<Badge>[]> {
    return this.http
      .get<BadgeContainer<Badge>[]>(this.OTM_INSTANCE + '/api/v1/gamification/badgeContainers', {
        params: {
          type
        }
      })
      .pipe(
        map((response: any) => {
          const bcList = [];
          response.forEach(bc => {
            bcList.push(BadgeContainer.fromJson(bc));
          });
          return bcList;
        })
      );
  }
}
