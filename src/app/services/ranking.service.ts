import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MainPersona} from '../models/user-profile';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Ranking} from '../models/ranking';

@Injectable({
    providedIn: 'root'
})
export class RankingService {
    OTM_INSTANCE = environment.OTM_INSTANCE;

    constructor(private http: HttpClient) {
    }

    getRanking(variable: string, page: number): Observable<Ranking> {
        return this.http.get<Ranking>(this.OTM_INSTANCE + '/api/v1/gamification/ranking',
        // return this.http.get<Ranking>('https://dev.co3.ontomap.eu/api/v1/gamification/ranking',
            {
                headers: {
                    Authorization: 'Bearer ' + environment.USER_AUTH.access_token
                },
                params: {
                    variable,
                    page: page.toString(),
                    pageSize: '10'
                }
            });
    }
}
