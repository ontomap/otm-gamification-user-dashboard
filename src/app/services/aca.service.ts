import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ACA} from '../models/aca';
import {map} from 'rxjs/operators';
import {AcaProfile} from '../models/aca-profile';

@Injectable({
  providedIn: 'root'
})
export class AcaService {

    constructor(private http: HttpClient) {
    }

    getAcas(): Observable<ACA[]> {
        return this.http.get(environment.FL_INSTANCE + '/v6/fl/Things/search?types=CO3_ACA').pipe(
            map((response: any) => {
                return response.things.features.flatMap(feature => {
                    const aca = new ACA();
                    aca.name = feature.properties.name;
                    aca.url = feature.self;
                    return aca;
                });
            })
        );
    }

    getAca(url: string): Observable<ACA> {
        return this.http.get(url).pipe(
            map((response: any) => {
                return response.map(feature => {
                    const aca = new ACA();
                    aca.name = feature.properties.name;
                    aca.url = feature.self;
                    return aca;
                });
            })
        );
    }

    getAcaProfile(): Observable<AcaProfile[]> {
        return this.http
            .get<AcaProfile>(environment.OTM_INSTANCE + '/api/v1/gamification/profile/aca',{
                headers: {
                    Authorization: 'Bearer ' + environment.USER_AUTH.access_token
                },
            })
            .pipe(
                map((response: any) => {
                    return  response ? response.map(aca => AcaProfile.fromJson(aca)) : null;
                })
            );

    }
}
