import { TestBed } from '@angular/core/testing';

import { AcaService } from './aca.service';

describe('AcaService', () => {
  let service: AcaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AcaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
