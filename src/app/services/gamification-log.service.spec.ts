import { TestBed } from '@angular/core/testing';

import { GamificationLogService } from './gamification-log.service';

describe('GamificationLogService', () => {
  let service: GamificationLogService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GamificationLogService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
