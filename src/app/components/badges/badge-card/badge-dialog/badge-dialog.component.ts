import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Badge, BadgeStep, VariablePrecondition} from '../../../../models/badge';
import {UserProfile} from '../../../../models/user-profile';
import {AcaProfile} from '../../../../models/aca-profile';
import {AcaBadgeContainer} from '../../../../models/badge-container';

@Component({
    selector: 'app-badge-dialog',
    templateUrl: './badge-dialog.component.html',
    styleUrls: ['./badge-dialog.component.scss']
})
export class BadgeDialogComponent implements OnInit {
    badge: Badge;
    badgeList: Badge [];
    userProfile: UserProfile;
    acaProfile: AcaProfile;
    variablePreconditions: VariablePreconditionCheck[];

    constructor(public dialog: MatDialog, public dialogRef: MatDialogRef<BadgeDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.badge = data.badge;
        this.badgeList = data.badgeList;
        this.userProfile = data.userProfile;
        this.acaProfile = data.acaProfile;
        console.log(this);
    }

    ngOnInit(): void {
        const vars = this.asBadgeStep.preconditions.variables;
        this.variablePreconditions = [];
        if (vars) {
            for (const v of vars) {
                const vc = new VariablePreconditionCheck();
                vc.variable = v.variable;
                vc.threshold = v.threshold;
                vc.userValue = this.getVariableValue(v.variable);
                vc.satisfied = vc.userValue >= vc.threshold;
                this.variablePreconditions.push(vc);
            }
        }
    }

    getVariableValue(v: string): number {
        const bc = this.badge.badgeContainer;
        if (bc instanceof AcaBadgeContainer) {
            // tslint:disable-next-line:no-bitwise
            return this.acaProfile.getContainer(bc.timeframe).variables[v] | 0;
        } else {
            return this.userProfile.getFullVariable(v);
        }
    }

    hasBadge(idBadge: string): boolean{
        const bc = this.badge.badgeContainer;
        if (bc instanceof AcaBadgeContainer) {
            return this.acaProfile.hasBadge(idBadge);
        } else {
            return this.userProfile.hasBadge(idBadge);
        }
    }

    get isBadgeStep(): boolean {
        return this.badge instanceof BadgeStep;
    }

    get asBadgeStep(): BadgeStep {
        return this.badge as BadgeStep;
    }

    numberWithPoints(x): string {
        // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
        return x;
    }

    getBadgeById(idBadge): Badge {
        return this.badgeList.find(badge => idBadge === badge.id);
    }

    openBadgePreconditionDialog(badgePrec): void {
        if (badgePrec) {
            const dialogRef = this.dialog.open(BadgeDialogComponent, {
                data: {
                    badge: badgePrec,
                    badgeList: this.badgeList,
                    userProfile: this.userProfile,
                    acaProfile: this.acaProfile
                },
                panelClass: 'badge-dialog',
                maxWidth: '100%'
            });
        }
    }

}

export class VariablePreconditionCheck extends VariablePrecondition {
    satisfied: boolean;
    userValue: number;
}
