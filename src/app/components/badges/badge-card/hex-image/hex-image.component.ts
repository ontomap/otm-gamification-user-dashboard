import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-hex-image',
  templateUrl: './hex-image.component.html',
  styleUrls: ['./hex-image.component.css']
})
export class HexImageComponent implements OnInit {

  @Input() src: string;

  constructor() { }

  ngOnInit(): void {
  }

}
