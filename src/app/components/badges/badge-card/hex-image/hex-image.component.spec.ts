import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HexImageComponent } from './hex-image.component';

describe('HexImageComponent', () => {
  let component: HexImageComponent;
  let fixture: ComponentFixture<HexImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HexImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HexImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
