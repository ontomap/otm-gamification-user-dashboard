import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {BadgeDialogComponent} from './badge-dialog/badge-dialog.component';
import {Badge} from '../../../models/badge';
import {UserProfile} from '../../../models/user-profile';
import {TemporalBadgeContainer} from '../../../models/badge-container';
import {AcaProfile} from '../../../models/aca-profile';

@Component({
    selector: 'app-badge-card',
    templateUrl: './badge-card.component.html',
    styleUrls: ['./badge-card.component.scss']
})
export class BadgeCardComponent implements OnInit {

    @Input() badge: Badge;
    @Input() badgeList: Badge[];
    @Input() earnedDate: Date;
    @Input() userProfile: UserProfile;
    @Input() acaProfile: AcaProfile;

    constructor(public dialog: MatDialog) {
        this.earnedDate = new Date();
    }

    ngOnInit(): void {
    }

    get completed(): boolean {
        return this.badge.completion === 100;
    }

    get stringDate(): string {
        if (!this.earnedDate) {
            return '';
        }
        return this.earnedDate.getDate() + '/' + this.earnedDate.getMonth() + '/' + this.earnedDate.getFullYear();
    }

    get timeframe(): string {
        if (this.badge.badgeContainer instanceof TemporalBadgeContainer) {
            const timeframe = this.badge.badgeContainer.timeframe;
            return timeframe === 'WEEK' ? 'Weekly' : 'Monthly';
        }
        return null;
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(BadgeDialogComponent, {
            data: {
                badge: this.badge,
                badgeList: this.badgeList,
                userProfile: this.userProfile,
                acaProfile: this.acaProfile
            },
            panelClass: 'badge-dialog',
            maxWidth: '100%'
        });
    }

}
