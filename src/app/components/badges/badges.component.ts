import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {
    AcaBadgeContainer,
    BadgeContainer,
    IndividualBadgeContainer,
    RankingBadgeContainer,
    TemporalBadgeContainer
} from '../../models/badge-container';
import {Badge, BadgePosition, BadgeStep} from '../../models/badge';
import {UserProfile} from '../../models/user-profile';
import {BadgesFilterDialogComponent} from './badges-filter-dialog/badges-filter-dialog.component';
import {MatDialog} from '@angular/material/dialog';
import {AcaProfile} from '../../models/aca-profile';

@Component({
    selector: 'app-badges',
    templateUrl: './badges.component.html',
    styleUrls: ['./badges.component.css']
})
export class BadgesComponent implements OnInit, OnChanges {

    @Input() userProfile: UserProfile;
    @Input() badgeContainerList: BadgeContainer<any>[];
    @Input() acaProfile: AcaProfile;
    individualBadgeList: BadgeStep[] = [];
    acaBadgeList: BadgeStep[] = [];
    rankingBadgeList: BadgePosition[] = [];
    badgeList: Badge[];

    timeframe: string;

    hideCompleted = true;
    type: string;
    searchText = '';


    constructor(public dialog: MatDialog) {

    }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.badgeContainerList && changes.badgeContainerList.currentValue) {
            const individualBadgeList = [];
            const acaBadgeList = [];
            const rankingBadgeList = [];
            changes.badgeContainerList.currentValue.forEach(badgeContainer => {
                let i;
                for (i = 0; i < badgeContainer.getBadgeList().length; i++) {
                    const badge = badgeContainer.getBadgeList()[i];
                    if (badge instanceof BadgeStep && badgeContainer instanceof IndividualBadgeContainer) {
                        badge.completion = this.calcCompletionBadgeStepIndividual(badge);
                        if (badge.completion === 100) {
                            badge.isNextStep = false;
                            if (badge instanceof BadgeStep) {
                                let nextStep;
                                if ((i + 1) < badgeContainer.getBadgeList().length) {
                                    nextStep = badgeContainer.getBadgeList()[i + 1];
                                    nextStep.isNextStep = true;
                                }
                            }
                        }
                    } else if (badge instanceof BadgeStep && badgeContainer instanceof AcaBadgeContainer) {
                        badge.completion = 33;
                    }

                    if (badgeContainer instanceof IndividualBadgeContainer) {
                        individualBadgeList.push(badge);
                    } else if (badgeContainer instanceof AcaBadgeContainer) {
                        badge.completion = this.calcCompletionBadgeStepAca(badge);
                        acaBadgeList.push(badge);
                    } else if (badgeContainer instanceof RankingBadgeContainer) {
                        rankingBadgeList.push(badge);
                    }
                }

                this.individualBadgeList = individualBadgeList;
                this.acaBadgeList = acaBadgeList;
                this.rankingBadgeList = rankingBadgeList;
                this.badgeList = (this.individualBadgeList as Badge[]).concat(this.acaBadgeList).concat(this.rankingBadgeList);
                this.badgeList = this.badgeList.sort((a, b) => b.completion - a.completion);
            });
        }
    }

    get filteredBadgeList(): Badge[] {
        return this.badgeList.filter(badge => {
            return this.filteredByName(badge) && (!this.hideCompleted || (badge.completion < 100)) &&
                this.filteredByContainer(badge) && this.filteredByTimeframe(badge);
        });
    }

    filteredByName(badge: Badge): boolean {
        return (badge.name.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1 ||
            badge.badgeContainer.name.toLowerCase().indexOf(this.searchText.toLowerCase()) > -1);
    }

    filteredByTimeframe(badge: Badge): boolean {
        if (!this.timeframe) {
            return true;
        } else if (badge.badgeContainer instanceof TemporalBadgeContainer) {
            return this.timeframe === badge.badgeContainer.timeframe;
        }
        return false;
    }

    filteredByContainer(badge: Badge): boolean {
        switch (this.type) {
            case null:
                return true;
            case 'individual':
                return badge.badgeContainer instanceof IndividualBadgeContainer;
            case 'aca':
                return badge.badgeContainer instanceof AcaBadgeContainer;
            case 'ranking':
                return badge.badgeContainer instanceof RankingBadgeContainer;
            default:
                return true;

        }
    }

    calcCompletionBadgeStepIndividual(badge: BadgeStep): number {
        if (this.userProfile.hasBadge(badge.id)) {
            return 100;
        }
        const preconditions = badge.preconditions;
        let base = 0;
        let completion = 0;
        const aca = badge.badgeContainer.aca;
        if (preconditions.badgeList) {
            base += preconditions.badgeList.length;
            preconditions.badgeList.forEach(idBadgePrecondition => {
                completion += this.userProfile.hasBadge(idBadgePrecondition) ? 1 : 0;
            });
        }
        if (preconditions.variables) {
            base += preconditions.variables.length;
            preconditions.variables.forEach(variable => {
                const varName = variable.variable;
                const varValue = variable.threshold;
                const persona = this.userProfile.getPersona(aca);
                const acquiredValue = persona.getFullVariable(varName);
                if (acquiredValue >= varValue) {
                    completion += 1;
                } else {
                    completion += (acquiredValue / varValue);
                }
            });
        }
        return Math.round((completion / base) * 100);
    }

    calcCompletionBadgeStepAca(badge: BadgeStep): number {
        const badgeContainer = badge.badgeContainer as AcaBadgeContainer;
        const timeframe = badgeContainer.timeframe;
        const preconditions = badge.preconditions;
        let base = 0;
        let completion = 0;
        if (preconditions.badgeList) {
            base += preconditions.badgeList.length;
            preconditions.badgeList.forEach(idBadgePrecondition => {
                completion += this.acaProfile.hasBadge(idBadgePrecondition) ? 1 : 0;
            });
        }
        if (preconditions.variables) {
            base += preconditions.variables.length;
            preconditions.variables.forEach(variable => {
                const varName = variable.variable;
                const varValue = variable.threshold;
                const variableContainer = this.acaProfile.getContainer(timeframe);
                // tslint:disable-next-line:no-bitwise
                const acquiredValue = variableContainer ? variableContainer.variables[varName] | 0 : 0;

                if (acquiredValue >= varValue) {
                    completion += 1;
                } else {
                    completion += (acquiredValue / varValue);
                }
            });
        }
        return Math.round((completion / base) * 100);
    }

    openFilters(): void {
        const dialogRef = this.dialog.open(BadgesFilterDialogComponent, {
            maxWidth: '80%',
            data: {hideCompleted: this.hideCompleted, type: this.type, timeframe: this.timeframe}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.type = result.type;
                this.hideCompleted = result.hideCompleted;
                this.timeframe = result.timeframe;
            }
        });
    }
}
