import {Component, Inject, OnInit} from '@angular/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {Moment} from 'moment/moment';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-badges-filter-dialog',
    templateUrl: './badges-filter-dialog.component.html',
    styleUrls: ['./badges-filter-dialog.component.css']
})
export class BadgesFilterDialogComponent implements OnInit {

    hideCompleted = true;
    type: string;
    timeframe: string;
    timeframeFormControl: FormControl;
    categoryFormControl: FormControl;

    constructor(@Inject(MAT_DIALOG_DATA) public input: any) {
        this.hideCompleted = input.hideCompleted;
        this.type = input.type;
        this.timeframe = input.timeframe;

        this.categoryFormControl = new FormControl(this.type, []);
        this.timeframeFormControl = new FormControl(this.timeframe, []);
        if (!this.type || this.type === 'individual') {
            this.timeframeFormControl.disable();
        }
        this.categoryFormControl.valueChanges.subscribe(newValue => {
            this.type = newValue;
            if (!this.type || this.type === 'individual') {
                this.timeframeFormControl.setValue(null);
                this.timeframeFormControl.disable();
            } else {
                this.timeframeFormControl.enable();
                this.timeframeFormControl.setValue('MONTH');
            }
        });
        this.timeframeFormControl.valueChanges.subscribe(newValue => {
            this.timeframe = newValue;
        });
    }

    ngOnInit(): void {

    }

    get data(): any {
        return {hideCompleted: this.hideCompleted, type: this.type, timeframe: this.timeframe};
    }

}
