import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgesFilterDialogComponent } from './badges-filter-dialog.component';

describe('BadgesFilterDialogComponent', () => {
  let component: BadgesFilterDialogComponent;
  let fixture: ComponentFixture<BadgesFilterDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BadgesFilterDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgesFilterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
