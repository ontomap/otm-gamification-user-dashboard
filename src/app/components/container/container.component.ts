import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {UserProfileService} from '../../services/user-profile.service';
import {environment} from '../../../environments/environment';
import {UserProfile} from '../../models/user-profile';
import {BadgeService} from '../../services/badge.service';
import {AcaBadgeContainer, BadgeContainer, IndividualBadgeContainer, RankingBadgeContainer} from '../../models/badge-container';
import {Badge, BadgeStep} from '../../models/badge';
import {AcaService} from '../../services/aca.service';
import {ACA} from '../../models/aca';
import {AcaProfile} from '../../models/aca-profile';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ContainerComponent implements OnInit {

    windows = true;
    userProfile: UserProfile;
    badgeContainerList: BadgeContainer<any>[];

    filteredBadgeContainerList: BadgeContainer<any>[];
    rankingBadgeList: RankingBadgeContainer[];
    levelsBadgeContainer: IndividualBadgeContainer;

    acaList: ACA[];
    acaSelected =  'pilot';

    acaProfileDict: { [id: string]: AcaProfile };
    acaProfile: AcaProfile;
    variables: string[] = [];

    acaFormControl: FormControl;

    constructor(private badgeService: BadgeService, private userProfileService: UserProfileService, private acaService: AcaService) {
        this.acaFormControl = new FormControl(this.acaSelected);
        this.acaFormControl.valueChanges.subscribe(newValue => {
            this.acaSelected = newValue;
            this.acaProfile = this.acaProfileDict[newValue];
            this.filterBadges();
        });
    }

    ngOnInit(): void {
        this.badgeService.getBadgesAll().subscribe(badgeContainers => {
            const badgeContainerList = [];
            const variables = {};
            badgeContainers.forEach(b => {
                if (b.name === 'Levels') {
                    this.levelsBadgeContainer = b as IndividualBadgeContainer;
                }
                badgeContainerList.push(b);
                b.getVariables().forEach(v => {
                    variables[v] = true;
                });
            });
            this.badgeContainerList = badgeContainerList;
            this.variables = Object.keys(variables).sort((a, b) => a.localeCompare(b));
            this.filterBadges();
        });

        this.userProfileService.getProfile('hierarchy').subscribe(data => {
            this.userProfile = data;
        });
        this.acaService.getAcas().subscribe(data => {
            this.acaList = data;
        });

        this.acaService.getAcaProfile().subscribe(data => {
            const acaProfileDict = {};
            data.forEach(acaProfile => {
                acaProfileDict[acaProfile.url] = acaProfile;
            });
            this.acaProfileDict = acaProfileDict;
            console.log(data);
            console.log(this.acaSelected);
            this.acaProfile = this.acaProfileDict[this.acaSelected];
        });
    }

    filterBadges(): void {
        const rankingBadgeList = [];
        const filteredBadgeContainerList = [];
        this.badgeContainerList.forEach(badgeContainer => {
            if (!this.acaProfile || this.acaProfile.url === 'pilot' || badgeContainer.aca === this.acaProfile.url){
                filteredBadgeContainerList.push(badgeContainer);
                if (badgeContainer instanceof RankingBadgeContainer) {
                    rankingBadgeList.push(badgeContainer);
                }
            }
        });

        this.rankingBadgeList = rankingBadgeList;
        this.filteredBadgeContainerList = filteredBadgeContainerList;
    }

}
