import {
    Component,
    OnInit,
    ViewChild,
    AfterViewInit,
    Input,
    OnChanges,
    SimpleChanges,
    AfterViewChecked,
    ChangeDetectorRef
} from '@angular/core';
import {Rank, Ranking} from '../../models/ranking';
import {RankingService} from '../../services/ranking.service';
import {merge, Observable, of as observableOf} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {BadgeService} from '../../services/badge.service';
import {RankingBadgeContainer} from '../../models/badge-container';
import {UserProfileService} from '../../services/user-profile.service';
import {FormControl, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {RulesInfoComponent} from './rules-info/rules-info.component';
import {UserProfile} from '../../models/user-profile';

@Component({
    selector: 'app-ranking',
    templateUrl: './ranking.component.html',
    styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit, AfterViewInit, OnChanges {
    displayedColumns: string[] = ['position', 'actor-name', 'value'];
    rankList: Rank[] = [];
    isLoadingResults = true;
    resultsLength = 0;
    @Input() rankingBadges: RankingBadgeContainer[] = [];
    @Input() actor?: number | -1;
    selectedRankingBadge: RankingBadgeContainer;
    selectedRanking: Ranking;
    namesMap: { [actor: string]: string } = {};

    rankingBadgeFormControl: FormControl;
    acaFormControl: FormControl;

    @ViewChild(MatPaginator) paginator: MatPaginator;


    constructor(public dialog: MatDialog, private rankingService: RankingService,
                private userProfileService: UserProfileService) {
        this.rankingBadgeFormControl = new FormControl(null, []);
        this.acaFormControl = new FormControl(null, []);
    }

    ngOnInit(): void {

    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.rankingBadges && changes.rankingBadges.currentValue) {
            this.rankingBadges = changes.rankingBadges.currentValue;
            this.rankingBadgeFormControl.setValue(this.rankingBadges[0]);
        }
    }

    ngAfterViewInit(): void {
        this.rankingBadgeFormControl.valueChanges.subscribe(newValue => {
            this.selectedRankingBadge = newValue;
            this.updateRanking();
        });
        setTimeout(() => {
            if (this.rankingBadges) {
                this.rankingBadgeFormControl.setValue(this.rankingBadges[0]);
            }
        });
    }

    get variable(): string {
        return this.selectedRankingBadge ? this.selectedRankingBadge.variable : null;
    }

    get endingDate(): string {
        if (this.selectedRanking) {
            const date = new Date(this.selectedRanking.endingDate);
            return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' +
                '' + date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        }
        return null;
    }

    get timeframe(): string {
        if (this.selectedRanking) {
            return this.selectedRanking.timeframe === 'WEEK' ? 'Weekly' : 'Monthly';
        }
        return null;
    }


    updateUserNames(): void {
        const actorsDict = {};
        this.rankList.forEach(rank => {
            actorsDict[rank.actor] = true;
        });
        this.userProfileService.getUserName(Object.keys(actorsDict)).subscribe(UUMs => {
            UUMs.forEach(uum => {
                // tslint:disable-next-line:radix
                this.namesMap[parseInt(uum.id)] = uum.name;
            });
        });
    }

    updateRanking(): void {
        this.rankList = [];
        merge(this.paginator.page
        )
            .pipe(
                startWith({}),
                switchMap(() => {
                    this.isLoadingResults = true;
                    return this.rankingService.getRanking(this.variable, this.paginator.pageIndex);
                }),
                map(data => {
                    // Flip flag to show that loading has finished.
                    this.isLoadingResults = false;
                    this.resultsLength = data.totalCount;
                    this.selectedRanking = data;
                    return data.rankList;
                }),
                catchError(() => {
                    this.isLoadingResults = false;
                    // Catch if the GitHub API has reached its rate limit. Return empty data.
                    return observableOf([]);
                })
            ).subscribe(data => {
            this.rankList = data;
            this.updateUserNames();
        });
    }

    openVariableInfo(): void {
        this.dialog.open(RulesInfoComponent, {
            data: this.variable,
            panelClass: 'variable-info-dialog'
        });
    }
}
