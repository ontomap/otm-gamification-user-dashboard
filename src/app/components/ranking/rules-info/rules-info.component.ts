import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-rules-info',
  templateUrl: './rules-info.component.html',
  styleUrls: ['./rules-info.component.css']
})
export class RulesInfoComponent implements OnInit {
  variable: string;

  constructor( @Inject(MAT_DIALOG_DATA) public data: any) {
    this.variable = data;
  }

  ngOnInit(): void {
  }

}
