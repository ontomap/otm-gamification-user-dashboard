import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesInfoComponent } from './rules-info.component';

describe('RulesInfoComponent', () => {
  let component: RulesInfoComponent;
  let fixture: ComponentFixture<RulesInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RulesInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
