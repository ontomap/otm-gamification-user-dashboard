import {Component, Inject, OnInit} from '@angular/core';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {Moment} from 'moment/moment';
import {ACA} from '../../../models/aca';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'app-logs-filter',
    templateUrl: './logs-filter.component.html',
    styleUrls: ['./logs-filter.component.css']
})
export class LogsFilterComponent implements OnInit {

    today: Date;
    fromDate: Date;
    untilDate: Date;
    // acaList: ACA[];
    // aca: ACA;
    variables: string[];
    variable: string;

    constructor(@Inject(MAT_DIALOG_DATA) public input: any) {
        this.fromDate = input.fromDate;
        this.untilDate = input.untilDate;
        // this.acaList = input.acaList;
        // this.aca = input.aca;
        this.variables = input.variables;
        this.variable = input.variable;
    }

    ngOnInit(): void {
        this.today = new Date();
    }

    get data(): any {
        return {
            fromDate: this.fromDate,
            untilDate: this.untilDate,
            // aca: this.aca,
            variable: this.variable
        };
    }

    intervalChanged(dateType: string, $event: MatDatepickerInputEvent<Moment>): void {
        if (dateType === 'from') {
            if ($event.value) {
                this.fromDate = $event.value.toDate();
            } else {
                this.fromDate = null;
            }
        } else {
            if ($event.value) {
                this.untilDate = $event.value.toDate();
                this.untilDate.setDate(this.untilDate.getDate() + 1);
            } else {
                this.untilDate = null;
            }
        }
        // if (dateType === 'until' && this.untilDate) {
        //     this.resetLogs();
        //     this.loadLogs();
        // }
    }

}
