import {
    ChangeDetectionStrategy,
    Component,
    ElementRef,
    HostListener,
    Input,
    OnChanges,
    OnInit, SimpleChanges,
    ViewChild,
    ViewChildren
} from '@angular/core';
import {GamificationLog, RuleLog} from '../../models/gamification-log';
import {Rule} from '../../models/rule';
import {GamificationLogService} from '../../services/gamification-log.service';
import {DatePipe} from '@angular/common';
import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {Moment} from 'moment/moment';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {LogsFilterComponent} from './logs-filter/logs-filter.component';
import {MatDialog} from '@angular/material/dialog';
import {ACA} from '../../models/aca';


export const MY_FORMATS = {
    parse: {
        dateInput: 'LL',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY',
    },
};

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.css'],
    providers: [DatePipe, {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}]
})
export class HistoryComponent implements OnInit, OnChanges {
    logs: GamificationLog[] = [];
    dateLogsList: DateLogs[] = [];
    todayDateString: string;
    yesterdayDateString: string;

    page = 0;
    pageSize = 50;

    today: Date;
    @Input() selectedAca: string;
    @Input() variables: string[];

    fromDate: Date;
    untilDate: Date;
    variable: string = null;


    constructor(private glogService: GamificationLogService, public dialog: MatDialog, private datePipe: DatePipe, private el: ElementRef) {
        this.today = new Date();
        this.todayDateString = this.dateToString(this.today);
        this.today.setDate(this.today.getDate() - 1);
        this.yesterdayDateString = this.dateToString(this.today);
    }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.selectedAca.currentValue) {
            this.selectedAca = changes.selectedAca.currentValue;
            this.resetLogs();
            this.loadLogs();
        }
    }

    resetLogs(): void {
        this.logs = [];
        this.dateLogsList = [];
        this.page = 0;
    }

    loadLogs(): void {
        this.glogService.getLogs(this.variable, this.selectedAca, true, true, this.fromDate,
            this.untilDate, this.page, this.pageSize).subscribe(data => {
            this.page++;
            this.logs = data.results;
            this.logs.forEach(log => {
                const key = this.getDateString(log.date);
                let dl = this.dateLogsList.find(d => d.dateString === key);
                if (!dl) {
                    dl = new DateLogs();
                    dl.dateString = key;
                    dl.date = log.date;
                    this.dateLogsList.push(dl);
                }
                dl.addLog(log);
            });
            this.dateLogsList = this.dateLogsList.sort((a, b) => b.date.getTime() - a.date.getTime());
        });
    }

    dateToString(date: Date): string {
        // return date.getDate().toString() + '/' + date.getMonth().toString() + '/' + date.getFullYear().toString();
        return this.datePipe.transform(date, 'dd MMMM yyyy');
    }

    getDateString(date: Date): string {
        const s = this.dateToString(date);
        if (s === this.todayDateString) {
            return 'Today';
        }
        if (s === this.yesterdayDateString) {
            return 'Yesterday';
        }
        return s;
    }

    onScrollDown(ev): void {
        if (!this.el.nativeElement.parentElement) {
            return null;
        }
        this.loadLogs();
    }

    onUp(ev): void {
    }


    openFilters(): void {
        const dialogRef = this.dialog.open(LogsFilterComponent, {
            maxWidth: '80%',
            data: {
                fromDate: this.fromDate,
                untilDate: this.untilDate,
                variables: this.variables,
                variable: this.variable
            }
        });
        dialogRef.afterClosed().subscribe(data => {
            if (data) {
                this.fromDate = data.fromDate;
                this.untilDate = data.untilDate;
                this.variable = data.variable;
                this.ngOnInit();
            }
        });
    }
}

export class DateLogs {
    dateString: string;
    date: Date;
    private logs: GamificationLog[] = [];
    private sorted = false;

    addLog(log: GamificationLog): void {
        this.sorted = false;
        this.logs.push(log);
    }

    getLogs(): GamificationLog[] {
        if (!this.sorted) {
            this.logs = this.logs.sort((a: GamificationLog, b: GamificationLog) => {
                return b.date.getTime() - a.date.getTime();
            });
            this.sorted = true;
        }
        return this.logs;
    }
}



