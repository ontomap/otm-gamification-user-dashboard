import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateLogComponent } from './date-log.component';

describe('DateLogComponent', () => {
  let component: DateLogComponent;
  let fixture: ComponentFixture<DateLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
