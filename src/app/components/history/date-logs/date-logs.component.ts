import {Component, ElementRef, Input, OnInit, EventEmitter, Output, HostListener} from '@angular/core';
import {DateLogs} from '../history.component';

@Component({
    selector: 'app-date-logs',
    templateUrl: './date-logs.component.html',
    styleUrls: ['./date-logs.component.css']
})
export class DateLogsComponent implements OnInit {
    @Input() dateLogs: DateLogs;
    @Output() sectionPosition = new EventEmitter();

    constructor(private element: ElementRef) {
    }

    ngOnInit(): void {

    }


}
