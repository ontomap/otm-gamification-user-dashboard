import {Component, Input, OnInit} from '@angular/core';
import {BadgeLog, GamificationLog, RuleLog} from '../../../models/gamification-log';

@Component({
    selector: 'app-entry-log',
    templateUrl: './entry-log.component.html',
    styleUrls: ['./entry-log.component.css']
})
export class EntryLogComponent implements OnInit {

    @Input() log: GamificationLog;


    constructor() {
    }

    ngOnInit(): void {
    }

    get isRuleLog(): boolean {
        return this.log instanceof RuleLog;
    }

    get asRuleLog(): RuleLog{
        return this.log as RuleLog;
    }

    get asBadgeLog(): BadgeLog{
        return this.log as BadgeLog;
    }

    getActivityUrl(): void {
        if (this.log instanceof RuleLog){
            const event = this.log.event;
            if (event.activity_objects) {
                const prop = event.activity_objects[0].properties;
                return prop.hasWebView ? prop.hasWebView : prop.external_url;
            }
        }
        return null;
    }
}
