import {Component, Input, OnInit} from '@angular/core';
import {BadgeService} from '../../services/badge.service';
import {Badge, BadgePosition, BadgeStep} from '../../models/badge';
import {UserProfileService} from '../../services/user-profile.service';
import {AcaPersona, Badges, MainPersona, UserProfile} from '../../models/user-profile';
import {environment} from '../../../environments/environment';
import {BadgeContainer, IndividualBadgeContainer} from '../../models/badge-container';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    @Input() badgeContainerLevel: IndividualBadgeContainer;
    @Input() userProfile: UserProfile;


    constructor() {
    }

    ngOnInit(): void {
    }

    get points(): number {
        if (this.userProfile) {
            return this.userProfile.getFullVariable('points');
        }
        return 0;
    }

    get kudos(): number {
        if (this.userProfile) {
            return this.userProfile.getFullVariable('kudos');
        }
        return 0;
    }
}
