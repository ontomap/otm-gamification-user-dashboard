import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
import {ThemePalette} from '@angular/material/core';
import {IndividualBadgeContainer} from '../../../models/badge-container';
import {MainPersona} from '../../../models/user-profile';
import {BadgeStep} from '../../../models/badge';

@Component({
    selector: 'app-badge-level',
    templateUrl: './badge-level.component.html',
    styleUrls: ['./badge-level.component.css']
})
export class BadgeLevelComponent implements OnInit, OnChanges {
    @Input() levelsContainer: IndividualBadgeContainer;
    @Input() userProfile: MainPersona;
    currentLevel: BadgeStep;
    nextLevel: BadgeStep;

    color: ThemePalette = 'primary';
    mode: ProgressSpinnerMode = 'determinate';
    diameter = 150;
    src: string;

    completionReady = () => 100;

    constructor() {
    }

    ngOnInit(): void {
        this.build();
    }

    ngOnChanges(changes: SimpleChanges): void {

    }

    private build(): void {
        let currentLevel = null;
        let i;
        let currentIndex = 0;
        for (i = 0; i < this.levelsContainer.getBadgeList().length; i++) {
            const level = this.levelsContainer.getBadgeList()[i];
            if (this.userProfile.hasBadge(level.id)) {
                currentLevel = level;
                currentIndex = i;
            } else {
                i = this.levelsContainer.getBadgeList().length;
            }
        }
        console.log(currentIndex);
        if (currentIndex <= this.levelsContainer.getBadgeList().length - 1) {
            this.nextLevel = this.levelsContainer.getBadgeList()[currentIndex + 1];
        }
        this.currentLevel = currentLevel;
        setTimeout(() => this.completionReady = () => this.completion, 0);
    }

    get completion(): number {
        return this.nextLevel ? this.nextLevel.completion : 100;
    }

}
