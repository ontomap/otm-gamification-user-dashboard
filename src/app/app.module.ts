import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ContainerComponent} from './components/container/container.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {ProfileComponent} from './components/profile/profile.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatRippleModule} from '@angular/material/core';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {RankingComponent} from './components/ranking/ranking.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RulesInfoComponent} from './components/ranking/rules-info/rules-info.component';
import {HistoryComponent} from './components/history/history.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {EntryLogComponent} from './components/history/entry-log/entry-log.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {DateLogsComponent} from './components/history/date-logs/date-logs.component';
import {LogsFilterComponent} from './components/history/logs-filter/logs-filter.component';
import {MatInputModule} from '@angular/material/input';
import {BadgeLevelComponent} from './components/profile/badge-level/badge-level.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSidenavModule} from '@angular/material/sidenav';
import {BadgesComponent} from './components/badges/badges.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {HexImageComponent} from './components/badges/badge-card/hex-image/hex-image.component';
import {ProgressBarComponent} from './components/badges/badge-card/progress-bar/progress-bar.component';
import {BadgeDialogComponent} from './components/badges/badge-card/badge-dialog/badge-dialog.component';
import {BadgeCardComponent} from './components/badges/badge-card/badge-card.component';
import { BadgesFilterDialogComponent } from './components/badges/badges-filter-dialog/badges-filter-dialog.component';

@NgModule({
    declarations: [
        AppComponent,
        ContainerComponent,
        ProfileComponent,
        BadgeCardComponent,
        BadgeDialogComponent,
        ProgressBarComponent,
        HexImageComponent,
        RankingComponent,
        RulesInfoComponent,
        HistoryComponent,
        EntryLogComponent,
        DateLogsComponent,
        LogsFilterComponent,
        BadgeLevelComponent,
        BadgesComponent,
        BadgesFilterDialogComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatTabsModule,
        FlexLayoutModule,
        MatIconModule,
        MatExpansionModule,
        HttpClientModule,
        MatCardModule,
        MatDialogModule,
        MatRippleModule,
        MatButtonModule,
        MatListModule,
        MatTableModule,
        MatPaginatorModule,
        MatSelectModule,
        MatFormFieldModule,
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatMomentDateModule,
        InfiniteScrollModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatSidenavModule,
        MatToolbarModule,
        MatSlideToggleModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
